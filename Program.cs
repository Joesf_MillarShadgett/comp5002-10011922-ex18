﻿using System;

namespace Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();

            /*
            NOTE: I had no idea exactly how you wanted these done Console
            I thought I'd have some fun with it and maybe learn something new.
             */
            
            Console.WriteLine("Ex18 - Part A\n");
            float[] aNumbers = new float[5] {1.0f, 6.0f, 2.1f, 7.3f, 3.6f};

            for (int i = 0; i < aNumbers.Length; i++)
            {
                Console.WriteLine($"The value of num{i+1} = {aNumbers[i]}");
            }

            // Part B
            Console.WriteLine("\nEx18 - Part B");
            Console.WriteLine("Note that we are using new numbers\n");
            float[] bNumbers = new float[6] {3.75f, 1.22f, 7.84f, 5.19f, 9.99f, 4.44f};

            for (int i = 0; i < (bNumbers.Length / 2); i++)
            {
                Console.WriteLine($"The value of num{i+1} + num{i+2} = {bNumbers[i] + bNumbers[i+1]}");
            }

            // Part C
            Console.WriteLine("\nEx18 - Part C");
            Console.WriteLine("Note that we are using new numbers\n");            
            float[] cNumbers = new float[4] {1.255f, 6.4321f, 4.44f, 9.99f};

            cNumbers[0] += cNumbers[1];
            cNumbers[2] += cNumbers[3];
            Console.WriteLine($"The value of num1 += num2 = {cNumbers[0]}");
            Console.WriteLine($"The value of num3 += num4 = {cNumbers[2]}");

            // Part D -_-
            Console.WriteLine("\nEx18 - Part D");
            //string myStr = "I am thinking...";
            // myStr *= 3;
            Console.WriteLine("Weyyy type mismatch");

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
